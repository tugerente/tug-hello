FROM python:3.11-slim-bullseye

WORKDIR /app

RUN pip install --no-cache Flask gunicorn psutil requests

COPY app.py .

CMD ["gunicorn", "app:app", "--timeout=60", "--workers=2", "--preload", "--bind=0.0.0.0:8000"]
