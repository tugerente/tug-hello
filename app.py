import os
import psutil
import logging

from datetime import datetime

from flask import Flask, render_template_string
import subprocess

index_tpl = """
<!DOCTYPE html>
<html>
  <head>
    <title>Hello 🐶!</title>
    <style>
      html, body { font-size: 14px }
      body {
        background-color: hsl(40, 100%, 92%);
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        color: #3a3a3a;
        text-align: center;
        padding-bottom: 10rem;
      }

      h1, h2 {
        font-size: 2rem;
        margin-top: 50px;
      }

      h2 {font-size: 1.25rem}

      table, pre {
        margin: 0 auto;
        width: 80%;
        max-width: 800px;
        text-align: left;

        border: 1px solid hsl(40, 100%, 85%);
        border-radius: 4px;
        box-shadow: 0 1px 5px -3px hsl(40, 20%, 50%);
      }

      table {
        border-collapse: collapse;
        border-spacing: 0;
        overflow: hidden;
      }

      th, td, pre {
        background:  hsl(40, 90%, 98%);
        padding: 5px 10px;
        font-size: .9rem;
        line-height: 1.25rem;
      }

      th, td {
        max-width: 300px;
        overflow-wrap: anywhere;
        word-wrap: break-word;
      }

      th {
        background-color: hsl(40, 100%, 85%);
        color: hsl(40, 100%, 40%);
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h1>Hello World!</h1>
    <p>Welcome to our cute little app 🐶!:</p>

    <h2>Environment</h2>
    <table>
      <thead>
        <tr>
          <th>Key</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        {% for key, value in env_vars %}
            <tr>
              <td>{{ key }}</td>
              <td>{% if 'SECRET' in key %}*****{% else %}{{ value }}{% endif %}</td>
            </tr>
        {% endfor %}
      </tbody>
    </table>

    <h2>Container</h2>
    <table>
      <tr>
        <th>Metric</th>
        <th>Value</th>
      </tr>
      {% for key, value in container.items() %}
      <tr>
        <td>{{ key }}</td>
        <td>{{ value }}</td>
      </tr>
      {% endfor %}
    </table>

    <h2>ECS Metadata</h2>
    <pre>{{ecs_metadata|tojson(indent=2)|safe }}</pre>

    <h2>Task Memory</h2>
    <table>
      <tr>
        <th>Metric</th>
        <th>Value</th>
      </tr>
      {% for key, value in task_info["memory"].items() %}
      <tr>
        <td>{{ key }}</td>
        <td>{{ value }}</td>
      </tr>
      {% endfor %}
    </table>

    <h2>Task Processes</h2>
    <table>
      <tr>
        <th>PID</th>
        <th>Name</th>
        <th>Command</th>
        <th>Memory RSS</th>
        <th>Create Time</th>
      </tr>
      {% for process in task_info["processes"] %}
        <tr>
          <td>{{ process["pid"] }}</td>
          <td>{{ process["username"]}}</td>
          <td>
            <strong>{{ process["name"] }}</strong>
            {{ process["cmdline"] }}
          </td>
          <td>{{ process["memory_usage"]}}</td>
          <td>{{ process["create_ago"] }}</td>
        </tr>
      {% endfor %}
    </table>
  </body>
</html>
"""

app = Flask(__name__)


def get_ecs_metadata() -> dict[str, str]:
    import requests

    ecs_metadata_uri = os.environ.get("ECS_CONTAINER_METADATA_URI_V4", None)

    if ecs_metadata_uri is None:
        logging.warning(f"ECS_CONTAINER_METADATA_URI_V4 Not in the environment")
        return {}

    try:
        response = requests.get(ecs_metadata_uri)
        response.raise_for_status()
        return response.json()
    except (requests.exceptions.RequestException, ValueError) as e:
        logging.warning(f"Could not retrieve ECS metadata: {e}")
        return {}


def get_available_memory():
    mem = psutil.Process().memory_info()
    vmem = psutil.virtual_memory()
    return {
        "Total Memory": psutil._common.bytes2human(vmem.total),
        "Available Memory": psutil._common.bytes2human(vmem.available),
        "Used Memory": psutil._common.bytes2human(mem.rss),
        "Free Memory": psutil._common.bytes2human(vmem.free),
    }


def get_container_context():
    is_running_in_container = os.path.exists("/.dockerenv")

    if not is_running_in_container:
        logging.info("Not found: /.dockerenv file")

        return {
            "Container ID": None,
            "Container Name": None,
            "Container IP Address": None,
            "Docker Image Name": None,
            "Docker Container Creation Time": None,
            "Docker Container Startup Time": None,
            "Docker Container Environment Variables": None,
            "Docker Container File System Paths": None,
        }

    container_id = (
        os.getenv("HOSTNAME")
        or subprocess.check_output(["cat", "/proc/self/cgroup"]).split(b"/")[-1].decode().strip()
    )

    container_name = os.getenv("HOSTNAME") or open("/etc/hostname", "r").read().strip()

    container_ip_address = (
        os.getenv("HOSTNAME")
        or subprocess.check_output(["ip", "addr", "show"])
        .decode()
        .split("inet ")[1]
        .split("/")[0]
        .strip()
    )

    docker_image_name = None

    try:
        cgroup = subprocess.check_output(["cat", "/proc/self/cgroup"])
        if cgroup == b"0::/\n":
            container_id = None
            docker_image_name = None
        else:
            container_id = os.getenv("HOSTNAME") or cgroup.split(b"/")[-1].decode().strip()
            docker_image_name = (
                os.getenv("IMAGE_NAME") or cgroup.split(b"/docker/")[1].split(b"/")[0].decode()
            )
    except subprocess.CalledProcessError:
        pass

    docker_container_creation_time = os.getenv("DOCKER_CREATED_AT")

    docker_container_startup_time = float(open("/proc/uptime", "r").read().split(" ")[0])

    docker_container_env_vars = dict(os.environ)

    docker_container_file_system_paths = {
        "Root": "/",
        "Current Working Directory": os.getcwd(),
    }

    context = {
        "Container ID": container_id,
        "Container Name": container_name,
        "Container IP Address": container_ip_address,
        "Docker Image Name": docker_image_name,
        "Docker Container Creation Time": docker_container_creation_time,
        "Docker Container Startup Time": docker_container_startup_time,
        "Docker Container Environment Variables": docker_container_env_vars,
        "Docker Container File System Paths": docker_container_file_system_paths,
    }

    return context


def format_timedelta(td):
    seconds = td.seconds
    minutes = seconds // 60
    hours = minutes // 60
    days = td.days
    if days > 0:
        return f"{days}d ago"
    elif hours > 0:
        return f"{hours}h ago"
    elif minutes > 0:
        return f"{minutes}m ago"
    else:
        return f"{seconds}s ago"


def get_running_processes():
    processes = []
    for pid in psutil.pids():
        try:
            process = psutil.Process(pid)
            create_time = datetime.fromtimestamp(process.create_time())
            time_since_create = datetime.now() - create_time
            time_since_create_str = format_timedelta(time_since_create)
            username = process.username()
            cmdline = " ".join(process.cmdline())
            memory_usage = psutil._common.bytes2human(process.memory_info().rss)

            processes.append(
                {
                    "pid": pid,
                    "name": process.name(),
                    "username": username,
                    "cmdline": cmdline,
                    "memory_usage": memory_usage,
                    "create_timestamp": time_since_create,
                    "create_ago": time_since_create_str,
                }
            )
        except (psutil.NoSuchProcess, psutil.AccessDenied):
            pass

    return sorted(processes, key=lambda x: x["create_timestamp"])


@app.route("/")
def landing():
    env_vars = [
        (key, value)
        for key, value in os.environ.items()
        if key.startswith("ECS") or key.startswith("TUG") or key in {"PORT", "ENVIRONMENT", "PATH"}
    ]

    task_info = {
        "memory": get_available_memory(),
        "processes": get_running_processes(),
    }

    container = get_container_context()
    ecs_metadata = get_ecs_metadata()

    return render_template_string(
        index_tpl,
        env_vars=env_vars,
        container=container,
        task_info=task_info,
        ecs_metadata=ecs_metadata,
    )


if __name__ == "__main__":
    app.run(
        debug=True,
        host="0.0.0.0",
        port=int(os.environ.get("PORT", 8000)),
    )
